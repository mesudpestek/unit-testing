﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Softray.Academy.Entities;

namespace Softray.Academy.Repository.Interfaces
{
    /// <summary>
    /// Represents an abstraction of the Intersections repository.
    /// </summary>
    public interface ICourseRepository : IRepository<Course>
    {
        /// <summary>
        /// Returns a boolean indicating whether an Intersection with the specified Id exists.
        /// </summary>
        /// <param name="id">Id of the Intersection.</param>
        /// <returns>Returns true if an Intersection with the specified Id exists, and false otherwise.</returns>
        Task<bool> CheckIfIntersectionExistsAsync(Guid id);
    }
}
