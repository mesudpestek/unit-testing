﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Softray.Academy.Entities;
using Softray.Academy.Repository.Interfaces;

namespace Softray.Academy.Repository
{
    /// <summary>
    /// Represents a MongoDB repository for Intersections.
    /// </summary>
    public class CoursesRepository : BaseRepository<Course>, ICourseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoursesRepository"/> class.
        /// </summary>
        /// <param name="mongoDatabase">A <see cref="IMongoDatabase"/> instance.</param>
        public CoursesRepository(IMongoDatabase mongoDatabase) 
            : base(mongoDatabase, "Intersections")
        {
        }

        /// <summary>
        /// Returns a boolean indicating whether an Intersection with the specified Id exists.
        /// </summary>
        /// <param name="id">Id of the Intersection.</param>
        /// <returns>Returns true if an Intersection with the specified Id exists, and false otherwise.</returns>
        public async Task<bool> CheckIfIntersectionExistsAsync(Guid id)
        {
            Course intersection = await GetAsync(id);

            if (intersection == null)
            {
                return false;
            }

            return true;
        }
    }
}
