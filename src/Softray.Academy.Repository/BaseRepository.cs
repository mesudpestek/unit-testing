﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Softray.Academy.Entities;

namespace Softray.Academy.Repository
{
    /// <summary>
    /// Generic base repository that provides CRUD operations on a MongoDB database.
    /// </summary>
    /// <typeparam name="TEntity">Represents the type of the entity that will be manipulated using the repository.</typeparam>
    public abstract class BaseRepository<TEntity> where TEntity : EntityBase
    {
        /// <summary>
        /// Gets or sets the MongoDB Collection that is going to be used by the <see cref="BaseRepository{TEntity}"/>.
        /// </summary>
        protected readonly IMongoCollection<TEntity> _collection;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="mongoDatabase">A <see cref="IMongoDatabase"/> instance.</param>
        /// <param name="collectionName">Name of the collection which will store the specified Entity.</param>
        protected BaseRepository(IMongoDatabase mongoDatabase, string collectionName)
        {
            _collection = mongoDatabase.GetCollection<TEntity>(collectionName);
        }
       
        /// <summary>
        /// Returns a list of all the objects inside the collection.
        /// </summary>
        /// <returns>List of all objects.</returns>
        public virtual Task<List<TEntity>> GetAsync()
        {
            return _collection.Find(entity => true).ToListAsync();
        }
        
        /// <summary>
        /// Returns an object with the specified id.
        /// </summary>
        /// <param name="id">Guid of the object that is to be returned.</param>
        /// <returns>An object of type TEntity.</returns>
        public virtual Task<TEntity> GetAsync(Guid id)
        {
            return _collection.Find(entity => entity.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Creates a new object inside the collection.
        /// </summary>
        /// <param name="entity">The object to be inserted.</param>
        /// <returns>The created object.</returns>
        public virtual Task CreateAsync(TEntity entity)
        {
            return _collection.InsertOneAsync(entity);
        }

        /// <summary>
        /// Updates an already existing object inside the collection.
        /// </summary>
        /// <param name="entityToUpdate">Updated version of the object. This object includes an Id which is used to identify which object to update inside the database.</param>
        /// <returns>The updated object.</returns>
        public virtual Task<TEntity> UpdateAsync(TEntity entityToUpdate)
        {
            return _collection.FindOneAndReplaceAsync<TEntity>(
                entity => entity.Id == entityToUpdate.Id, 
                entityToUpdate,
                new FindOneAndReplaceOptions<TEntity> { ReturnDocument = ReturnDocument.After });
        }

        /// <summary>
        /// Removes the object with the specified Id from the collection.
        /// </summary>
        /// <param name="id">Id of the object that is to be removed.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual Task RemoveAsync(Guid id)
        {
            var matchByIdFilter = Builders<TEntity>.Filter.Eq(entity => entity.Id, id);

            return _collection.DeleteOneAsync(matchByIdFilter);
        }
    }
}
