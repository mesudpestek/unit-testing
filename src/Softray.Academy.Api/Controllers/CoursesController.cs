﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Softray.Academy.Common;
using Softray.Academy.Entities;
using Softray.Academy.Repository;
using Softray.Academy.Repository.Interfaces;

namespace Softray.Academy.Api.Controllers
{
    [Route("api/courses")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseRepository _courseRepository;

        public CoursesController(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Course>> Get(Guid id)
        {
            try
            {
                var course = await _courseRepository.GetAsync(id);

                if (course == null)
                {
                    return NotFound();
                }

                return course;
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
