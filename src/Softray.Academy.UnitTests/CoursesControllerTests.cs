﻿using AutoFixture;
using Softray.Academy.Api.Controllers;
using Softray.Academy.Entities;
using Softray.Academy.Repository.Interfaces;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Softray.Academy.UnitTests
{
    public class CoursesControllerTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICourseRepository> _courseRepositoryMock;
        private readonly CoursesController _coursesController;

        public CoursesControllerTests()
        {
            _fixture = new Fixture();
            _courseRepositoryMock = new Mock<ICourseRepository>();
            _coursesController = new CoursesController(_courseRepositoryMock.Object);
        }

        [Fact]
        public async void GetCourseSuccess()
        {
            // Arrange
            var expectedResult = _fixture.Create<Course>();
            _courseRepositoryMock.Setup(repository => repository.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(expectedResult);

            // Act
            var result = await _coursesController.Get(It.IsAny<Guid>());

            // Assert
            _courseRepositoryMock.Verify(repository => repository.GetAsync(It.IsAny<Guid>()));
            result.Value
                  .Should()
                  .BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async void GetCourseFailedNotFound()
        {
            // Arrange
            _courseRepositoryMock.Setup(repository => repository.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            // Act
            var result = await _coursesController.Get(It.IsAny<Guid>());

            // Assert
            result.Result
                  .Should()
                  .NotBeNull()
                  .And
                  .BeOfType<NotFoundResult>();
        }

        [Fact]
        public async void GetCourseFailedByException()
        {
            // Arrange
            string exceptionMessage = _fixture.Create<string>();
            _courseRepositoryMock.Setup(repository => repository.GetAsync(It.IsAny<Guid>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            // Act
            var result = _coursesController.Get(It.IsAny<Guid>());

            // Assert
            result.Result
                  .Result
                  .Should()
                  .NotBeNull()
                  .And
                  .BeOfType<StatusCodeResult>()
                  .Which
                  .StatusCode
                  .Should()
                  .Be(500);
        }
    }
}
