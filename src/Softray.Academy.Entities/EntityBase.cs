﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace Softray.Academy.Entities
{
    /// <summary>
    /// Abstract class that lets the generic Base Repository use the Id property.
    /// </summary>
    public abstract class EntityBase
    {
        /// <summary>
        /// Gets or sets the Id (Guid) of the entity.
        /// </summary>
        /// <value>
        /// The Id (Guid) of the entity.
        /// </value>
        public Guid Id { get; set; }
    }
}
